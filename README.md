# Auto Ingest to Papad

The script checks if any new audio are generated from [Radio Pi](https://github.com/Pragathi-Foundation/Namdu1Radio) and does a couple of api calls.

The script works for multi cascading directories.

The generated file is checked if its actual file and if mp3 file it is sent to [Spano](https://gitlab.com/servelots/spano) that returns content addressed url_id. We take the required response and send along with json and send to [Papad-Api](https://github.com/janastu/papad-api) that ingests the audio file to [Papad](https://gitlab.com/servelots/slots/papad)
 
<h1 align="center">
  <img src="/assets/papadDashboard.png" width="500px" />
</h1>

[For more reference](https://pad.riseup.net/p/papad22)
