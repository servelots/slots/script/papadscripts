#!/bin/bash

#Configure the following variables
path="/var/www/html/.upload/"
uploadUrl="http://10.56.130.10:5000/"
baseUrl="http://10.56.130.10:9002/recordings/"
deviceName="Radio Pi"
currentTime=$(date)
time=$(date +"%D_%T")
description="Uploaded from $deviceName"
location="Halekote, Tumkur"
skill="Record DurgadahalliRadio $deviceName"
tags="Record DurgadahalliRadio $deviceName"

inotifywait -m -r $path -e close_write |
    while read dir action file; do
        echo "The file '$file' appeared in directory '$dir' via '$action'"
	if test -f $dir$file ; then
	    echo "The file '$file' is tested"
            if [[ $dir$file == *.mp3 ]]; then                
                spanoUrl=$(curl -H 'Allowed: application/json' -F "file=@$dir$file" $uploadUrl)

                spanoResponse=($(echo $spanoUrl | tr "/" "\n" | tail -n -1 | tr -d "}" | tr -d '"' ))
                type=($(echo $spanoResponse | tr "." "\n" | tail -n -1))

                curl -H "Content-Type: application/json"     -d '
		    	{
		    	"name": "'"$deviceName"'_'"$time"'",
		    	"contentUrl": "'"$spanoResponse"'",
		    	"embedUrl": "",
		    	"description": "'"$description"'",
		    	"station_name": "'"$deviceName"'",
	    		"created": "'"$currentTime"'",
		    	"type": "'"$type"'",
		    	"author": "'"$deviceName"' User",
			    "location": "'"$deviceName"' '"$location"'",
			    "skill": "'"$skill"'",
			    "tags": "'"$tags"'"}
			    '        $baseUrl        
            fi
        fi
done
